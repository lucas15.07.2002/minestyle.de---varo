package de.minestyle.varo.scheduler;

import de.minestyle.varo.Varo;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class TimeManager {

    private static int minutes = 0;

    public void startScheduler(final JavaPlugin javaPlugin){

        startScoreboardSheduler(javaPlugin);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(javaPlugin, () -> {

            if(!Varo.getInstance().getStarted())return;

            minutes++;

            for(Player all : Bukkit.getOnlinePlayers()){
                if(!Varo.playedTime.containsKey(all))Varo.playedTime.put(all, 0);

                int current = Varo.playedTime.get(all);
                Varo.playedTime.put(all, current+1);

                if(minutes == 5){
                    minutes = 0;
                    all.sendMessage(Varo.getInstance().getPrefix() + "§eDu wirst in §5" + (15-current) + " Minuten §egekickt");
                }

                if(current >= 15){
                    if(cankick(all)){
                        Varo.getInstance().getUtils().kick(all);
                    }
                }

            }

        }, 0, min);

    }

    private boolean cankick(final Player player){
        final Location loc = player.getLocation();

        for(Player all : Bukkit.getOnlinePlayers()){
            final Location allLoc = all.getLocation();
            if(loc.distanceSquared(allLoc) > 50){
                return true;
            }
            player.sendMessage(Varo.getInstance().getPrefix() + "§3Du bist zu Nah an einem Spieler um dich auszuloggen!");
            player.playSound(loc, Sound.NOTE_BASS, 10, 10);
            return false;
        }
        return false;

    }

    private int min = 20*60;
    private int sec = 20;

    public void startScoreboardSheduler(final JavaPlugin javaPlugin){
        Bukkit.getScheduler().scheduleAsyncRepeatingTask(javaPlugin, () -> {

            Varo.getInstance().getScoreboard().updateScoreboard();



        }, 0, 10);
    }

}
