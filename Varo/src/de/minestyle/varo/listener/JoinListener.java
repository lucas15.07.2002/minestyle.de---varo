package de.minestyle.varo.listener;

import de.minestyle.varo.Varo;
import de.minestyle.varo.utils.Playerdata;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(final PlayerJoinEvent event){
        final Player player = event.getPlayer();

        Varo.playedTime.put(player, 0);

        if(!Varo.getInstance().getPlayerConfig().playerExist(player)){
            Varo.getInstance().getPlayerConfig().createPlayer(player);
        }

        final String playername = player.getName();

        if(!Varo.getInstance().getBanManager().canJoin(playername)){
            event.setJoinMessage(null);
            Varo.getInstance().getBanManager().cancel(player);
            return;
        }

        final Playerdata playerdata = new Playerdata(player, Varo.getInstance().getPlayerConfig().getCanLogin(playername),
                false, Varo.getInstance().getPlayerConfig().getKills(playername), player.getUniqueId().toString(), playername,
                Varo.getInstance().getPlayerConfig().getTeam(playername), Varo.getInstance().getPlayerConfig().getTeamMate(playername));

        Varo.playerData.put(player, playerdata);

        event.setJoinMessage("§4+ §c" +player.getName() +  " §5hat den Server §2betreten§e.");
        Varo.getInstance().getScoreboard().setScoreboard(player);

        if(!Varo.getInstance().getStarted()){
            player.setGameMode(GameMode.ADVENTURE);
        }

        Varo.getInstance().getLocationManager().teleportToSpawn(player);

        if(Varo.getInstance().getPlayerConfig().isDead(playername)){
            player.kickPlayer(Varo.getInstance().getPrefix() + "§cDu bist ausgeschiden! Danke fürs Mitspielen");
            return;
        }

        if(Varo.getInstance().getPlayerConfig().getStrikes(playername) == 3){
            player.kickPlayer(Varo.getInstance().getPrefix() + "§4Du wurdest 3 Mal gestriked");
            return;
        }



    }

    @EventHandler
    public void onQuit(final PlayerQuitEvent event){
        final Playerdata playerdata = Varo.playerData.get(event.getPlayer());
        Varo.playerData.remove(event.getPlayer());

        if(Varo.getInstance().getStarted()){
            Varo.getInstance().getBanManager().quit(event.getPlayer().getName());
            Varo.getInstance().getPlayerConfig().savePlayerConfig(playerdata, event.getPlayer());

        }




    }




}
