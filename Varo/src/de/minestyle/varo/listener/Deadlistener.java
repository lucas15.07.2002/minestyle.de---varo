package de.minestyle.varo.listener;

import de.minestyle.varo.Varo;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Deadlistener implements Listener {

    @EventHandler
    public void onDeath(final PlayerDeathEvent event){
        final Player victim = event.getEntity();

        try{
            final Player killer = event.getEntity().getKiller();


            if(killer == null){
                event.setDeathMessage(Varo.getInstance().getPrefix() + victim.getName() + " §cwurde von §5" + killer.getName()
                        + " §6§lgetötet");


                final int current = Varo.getInstance().getPlayerConfig().getKills(killer.getName());
                Varo.getInstance().getPlayerConfig().getConfiguration().set(killer.getName() + ".kills", current+1);
                Varo.getInstance().getPlayerConfig().saveFile();

            }else {
                event.setDeathMessage(Varo.getInstance().getPrefix() + victim.getName() + " §cist §5gestorben");
            }
        }catch (NullPointerException e){
            event.setDeathMessage(Varo.getInstance().getPrefix() + victim.getName() + " §cist §5gestorben");
    }

        Varo.getInstance().getPlayerConfig().getConfiguration().set(victim.getName() + ".died", true);
        Varo.getInstance().getPlayerConfig().saveFile();

        victim.kickPlayer("§4Du bist gestorben!");

    }

}
