package de.minestyle.varo.commands;

import de.minestyle.varo.Varo;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NoVo implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {

        if(commandSender instanceof Player){

            final Player player = (Player)commandSender;
            if(player.hasPermission("Novo.admin")) {
                if (args.length == 0) {
                    commandSender.sendMessage(Varo.getInstance().getPrefix() + "§c/NoVo start");
                } else if (args[0].equalsIgnoreCase("start")) {
                    Varo.getInstance().getGamemanager().start();
                    commandSender.sendMessage(Varo.getInstance().getPrefix() + "§aDu hast das Spiel gestartet");
                }else if(args[0].equalsIgnoreCase("rt")) {

                    if(args.length == 5){
                        int teamNumber = 0;
                        String teamName, player1, player2;

                        try{
                            teamNumber = Integer.parseInt(args[1]);
                            if(Varo.getInstance().getConfigManager().teamNumberExist(teamNumber)){
                                player.sendMessage(Varo.getInstance().getPrefix() + "§cDas Team mit der Nummer " + teamNumber + " existiert schon!");
                                return false;
                            }
                        }catch (NumberFormatException e){
                            player.sendMessage(Varo.getInstance().getPrefix() + "§c" + args[1] + " ist keine Nummer!");
                        }

                        teamName = args[2];

                        if(Varo.getInstance().getConfigManager().teamNameExist(teamName)){
                            player.sendMessage(Varo.getInstance().getPrefix() + "§cDas Team mit dem Namen " + teamName + " existiert schon!");
                            return false;
                        }

                        player1 = args[3];
                        if(Varo.getInstance().getConfigManager().teamPlayerExist(player1)){
                            player.sendMessage(Varo.getInstance().getPrefix() + "§cDer Spieler " + player1 + " wurde schon configuriert!");
                            return false;
                        }

                        player2 = args[4];
                        if(Varo.getInstance().getConfigManager().teamPlayerExist(player2)){
                            player.sendMessage(Varo.getInstance().getPrefix() + "§cDer Spieler " + player2 + " wurde schon configuriert!");
                            return false;
                        }

                        Varo.getInstance().getConfigManager().registerTeam(teamNumber, teamName, player1, player2);

                        player.sendMessage(Varo.getInstance().getPrefix() + "§5Neues Team Regristriert!");
                        player.sendMessage(Varo.getInstance().getPrefix() + "§7Team-Nummer: §e" + teamNumber);
                        player.sendMessage(Varo.getInstance().getPrefix() + "§7Team-Name: §e" + teamName);
                        player.sendMessage(Varo.getInstance().getPrefix() + "§7Spieler 1: §e" + player1);
                        player.sendMessage(Varo.getInstance().getPrefix() + "§7Spieler 2: §e" + player2);

                    }else{
                        player.sendMessage(Varo.getInstance().getPrefix() + "§c/novo rt <teamNummer> <TeamName> <Spieler1> <Spieler2>");
                    }
                }else if(args[0].equalsIgnoreCase("pos")) {

                    //novo pos set/remove <TeamNumber> <TeamPlayer>

                    if(args.length == 4){

                        int teamnumber, playernumber;
                        teamnumber = -1;
                        playernumber = -1;

                        try{

                            teamnumber = Integer.parseInt(args[2]);
                            playernumber = Integer.parseInt(args[3]);

                        }catch (NumberFormatException e){
                            player.sendMessage(Varo.getInstance().getPrefix() + "§c/novo <set/remove> <TeamNummer> <PlayerNumber>");
                            return false;
                        }

                        if(Varo.getInstance().getConfigManager().teamNumberExist(teamnumber)){

                            if(playernumber == 1 || playernumber == 2){

                                if(Varo.getInstance().getLocationManager().setLocation(player, teamnumber, playernumber)){
                                    player.sendMessage(Varo.getInstance().getPrefix() + "§5 Spawn gesetzt!");
                                    player.sendMessage(Varo.getInstance().getPrefix() + "§7Teamnummer: §5" + teamnumber);
                                    player.sendMessage(Varo.getInstance().getPrefix() + "§7Playernumber: §5" + playernumber);
                                    return true;
                                }
                                player.sendMessage(Varo.getInstance().getPrefix() + "§4Es gab einen Fehler!");

                            }else {
                                player.sendMessage(Varo.getInstance().getPrefix() + "§cDie Spielernummer muss 1 oder 2 sein!");
                                player.sendMessage(Varo.getInstance().getPrefix() + "§c/novo <set/remove> <TeamNummer> <PlayerNumber>");
                                return false;
                            }

                        }else {
                            player.sendMessage(Varo.getInstance().getPrefix() + "§cDas Team mit der Zahl " + teamnumber + " existiert nicht!");
                            return false;
                        }


                    }else {
                        player.sendMessage(Varo.getInstance().getPrefix() + "§c/novo <set/remove> <TeamNummer> <PlayerNumber>");
                    }


                }else if(args[0].equalsIgnoreCase("strike")){
                   if(args.length == 3){
                       if(args[1].equalsIgnoreCase("add")){

                           final int curent = Varo.getInstance().getPlayerConfig().getStrikes(args[2]);
                           final int newCount = curent+1;

                           Varo.getInstance().getPlayerConfig().getConfiguration().set(args[2] + ".Strikes", newCount);
                           Varo.getInstance().getPlayerConfig().saveFile();

                           final Player target = Bukkit.getPlayerExact(args[2]);
                           if (target != null){
                               Varo.getInstance().getScoreboard().setScoreboard(target);
                               if(newCount == 3){
                                   player.kickPlayer("§cDu wurdest gestriket");
                               }else if(newCount == 1){
                                   Bukkit.broadcastMessage(Varo.getInstance().getPrefix() + "Coordinaten von " + target.getName() + ":");
                                   Bukkit.broadcastMessage(Varo.getInstance().getPrefix() + "X: " + target.getLocation().getX() + " Z: "
                                    +target.getLocation().getZ());
                               }else if(newCount == 2) {
                                   target.getInventory().clear();
                                   target.getInventory().setArmorContents(null);
                                   target.updateInventory();
                                   Bukkit.broadcastMessage(Varo.getInstance().getPrefix() + "§c" + target.getName() + " §cwurde Gestriked #2");
                               }
                           }else {
                               player.sendMessage(Varo.getInstance().getPrefix() + "§5§L" + args[2] + " §cist nicht online! Führe die Strafe manuel durch");
                           }

                           player.sendMessage(Varo.getInstance().getPrefix() + "§cDu hast " + args[2] + " gestriked.");


                       }else if(args[1].equalsIgnoreCase("remove")){
                           final int curent = Varo.getInstance().getPlayerConfig().getStrikes(args[2]);

                           if(curent == 0){
                               player.sendMessage(Varo.getInstance().getPrefix() + args[2] + " §chat keine Strikes");
                               return true;
                           }

                           final int newCount = curent-1;

                           Varo.getInstance().getPlayerConfig().getConfiguration().set(args[2] + ".Strikes", newCount);
                           Varo.getInstance().getPlayerConfig().saveFile();
                           Varo.getInstance().getScoreboard().setScoreboard(Bukkit.getPlayer(args[2]));

                            player.sendMessage(Varo.getInstance().getPrefix() + "§aDu hast 1. Strike von " + args[2] + " entfernt");
                           player.sendMessage(Varo.getInstance().getPrefix() + args[2] + " §chat " + newCount + " Strikes");
                       }else {
                           player.sendMessage(Varo.getInstance().getPrefix() + "§c/novo strike <add/remove> <playername>");
                       }
                   }else {
                       player.sendMessage(Varo.getInstance().getPrefix() + "§c/novo strike <add/remove> <playername>");
                   }
                }else{
                    commandSender.sendMessage(Varo.getInstance().getPrefix() + "§c/NoVo start");
                }
            }

        }else {
            if(args.length == 0){
                commandSender.sendMessage(Varo.getInstance().getPrefix() + "§c/NoVo start");
            }else if(args[0].equalsIgnoreCase("start")){
                Varo.getInstance().getGamemanager().start();
                commandSender.sendMessage(Varo.getInstance().getPrefix() +"§aDu hast das Spiel gestartet");
            }else {
                commandSender.sendMessage(Varo.getInstance().getPrefix() + "§c/NoVo start");
            }
        }

        return false;
    }
}
