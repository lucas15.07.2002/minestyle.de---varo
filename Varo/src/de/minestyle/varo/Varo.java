package de.minestyle.varo;

import de.minestyle.varo.commands.NoVo;
import de.minestyle.varo.listener.Deadlistener;
import de.minestyle.varo.listener.JoinListener;
import de.minestyle.varo.manager.*;
import de.minestyle.varo.scheduler.TimeManager;
import de.minestyle.varo.utils.Playerdata;
import de.minestyle.varo.utils.ScoreboardManager;
import de.minestyle.varo.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class Varo extends JavaPlugin {

    public static HashMap<Player, Integer> playedTime = new HashMap<>();
    public static HashMap<Player, Playerdata> playerData = new HashMap<>();
    private static Varo instance;

    private String prefix;
    private boolean started;

    private Utils utils;
    private ScoreboardManager scoreboard;
    private Gamemanager gamemanager;
    private ConfigManager configManager;
    private PlayerConfig playerConfig;
    private LocationManager locationManager;
    private BanManager banManager;

    private ItemStack adminMode;

    @Override
    public void onEnable() {
        instance = this;
        this.prefix = "§8[§4NoVo§8] §f";

        this.utils = new Utils();
        this.scoreboard = new ScoreboardManager();
        this.gamemanager = new Gamemanager();
        this.configManager = new ConfigManager();
        this.playerConfig = new PlayerConfig();
        this.locationManager = new LocationManager();
        this.banManager = new BanManager();

        this.adminMode = new ItemStack(Material.BARRIER);
        final ItemMeta itemMeta = adminMode.getItemMeta();
        itemMeta.setDisplayName("§c§lDu wurdest noch nicht konfiguriert!");
        adminMode.setItemMeta(itemMeta);

        this.init();
        playedTime.clear();
    }

    private void init(){
        final PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new JoinListener(), this);
        pluginManager.registerEvents(new Deadlistener(), this);

        getCommand("novo").setExecutor(new NoVo());

        new TimeManager().startScheduler(this);

        getGamemanager().createFile();
        setStarted(getGamemanager().getStartedFromFile());
        getPlayerConfig().createFile();
        getConfigManager().createFile();
        getLocationManager().createFile();
    }

    public static Varo getInstance() {
        return instance;
    }

    public String getPrefix() {
        return prefix;
    }

    public Utils getUtils() {
        return utils;
    }

    public ScoreboardManager getScoreboard() {
        return scoreboard;
    }

    public Gamemanager getGamemanager() {
        return gamemanager;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean getStarted(){
        return this.started;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public PlayerConfig getPlayerConfig() {
        return playerConfig;
    }

    public LocationManager getLocationManager() {
        return locationManager;
    }

    public BanManager getBanManager() {
        return banManager;
    }

    public ItemStack getAdminMode() {
        return adminMode;
    }

    public void setItem(final Player player){
        final Inventory inventory = player.getInventory();
        inventory.clear();
        for (int i = 0; i < 9; i++) {
            inventory.setItem(i, getAdminMode());
        }

        player.updateInventory();

    }
}

