package de.minestyle.varo.utils;

import de.minestyle.varo.Varo;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Utils {

    public void kick(final Player player){
        player.kickPlayer(Varo.getInstance().getPrefix() + "§cDeine Aufnahmezeit ist Aufgebraucht!");
    }

    public void sendActionBar(final Player player, final String msg){
        PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
        IChatBaseComponent chat = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + msg + "\"}");
        PacketPlayOutChat packetplayoutchat = new PacketPlayOutChat(chat, (byte) 2);
        connection.sendPacket(packetplayoutchat);
    }

    public void sendActionbarToAll(final String message){
        Bukkit.getOnlinePlayers().forEach(all -> sendActionBar(all, message));
    }

}
