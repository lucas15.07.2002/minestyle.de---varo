package de.minestyle.varo.utils;

import org.bukkit.entity.Player;

public class Playerdata {

    private Player player;
    private long lastLogin;
    private boolean died;
    private int kills;
    private String uuid, playerName, teamName, mateName;

    public Playerdata(Player player, long lastLogin, boolean died, int kills, String uuid, String playerName, String teamName, String mateName) {
        this.player = player;
        this.lastLogin = lastLogin;
        this.died = died;
        this.kills = kills;
        this.uuid = uuid;
        this.playerName = playerName;
        this.teamName = teamName;
        this.mateName = mateName;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(long lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isDied() {
        return died;
    }

    public void setDied(boolean died) {
        this.died = died;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getMateName() {
        return mateName;
    }

    public void setMateName(String mateName) {
        this.mateName = mateName;
    }
}
