package de.minestyle.varo.utils;

import de.minestyle.varo.Varo;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardManager {

    private int count = 0;

    public void setScoreboard(final Player player){
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("stats", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        int x, z;

        Team time = scoreboard.registerNewTeam("time" + player.getName());
        time.addEntry(ChatColor.YELLOW.toString());
        time.setSuffix("§cWAIT");
        time.setPrefix("     ");

        //Varo.playerData.get(player).getKills()
        Team kills = scoreboard.registerNewTeam("kills" + player.getName());
        kills.addEntry(ChatColor.BLUE.toString());
        kills.setSuffix("§cWAIT");
        kills.setPrefix("     ");

        if(Varo.getInstance().getStarted()){
            objective.getScore("§7Team:").setScore(11);
            objective.getScore("    §c" + Varo.playerData.get(player).getTeamName()).setScore(10);
            objective.getScore("§7").setScore(9);
            objective.getScore("§7Aufnahmezeit: ").setScore(8);
            objective.getScore(ChatColor.YELLOW.toString()).setScore(7);
            objective.getScore("§6").setScore(6);
            objective.getScore("§7Kills:").setScore(5);
            objective.getScore(ChatColor.BLUE.toString()).setScore(4);
            objective.getScore("§0").setScore(3);
            objective.getScore("§7Strikes:").setScore(2);
            objective.getScore("    §c" + Varo.getInstance().getPlayerConfig().getStrikes(player.getName()) + " Strikes").setScore(1);
        }else {
            objective.getScore("§7Status:").setScore(10);
            objective.getScore("    §cNicht gestartet").setScore(9);
        }

        objective.setDisplayName("§c§lN§4§loVo");
        player.setScoreboard(scoreboard);


    }

    public void updateScoreboard(){

        count++;
        String name = "";

        if(count == 30){
            name = "§c§lN§4§loVo";
        }else if(count == 31){
            name = "§4§lN§c§lo§4§lVo";
        }else if(count == 32){
            name = "§4§lNo§c§lV§4§lo";
        }else if(count == 33){
            name = "§4§lNoV§c§lo";
        }else if(count == 34){
            name = "§4§lNoVo";
            count = 0;
        }else {
            name = "§4§lNoVo";
        }

        String finalName = name;
        Bukkit.getOnlinePlayers().forEach(player -> {
            if(player.getScoreboard() == null){
                setScoreboard(player);
            }

            if(!Varo.playerData.containsKey(player)){
                final String playername = player.getName();
                final Playerdata playerdata = new Playerdata(player, Varo.getInstance().getPlayerConfig().getCanLogin(playername),
                        false, Varo.getInstance().getPlayerConfig().getKills(playername), player.getUniqueId().toString(), playername,
                        Varo.getInstance().getPlayerConfig().getTeam(playername), Varo.getInstance().getPlayerConfig().getTeamMate(playername));

                Varo.playerData.put(player, playerdata);
            }

            final Scoreboard scoreboard = player.getScoreboard();
            final Objective objective = scoreboard.getObjective(DisplaySlot.SIDEBAR);
            //NoVo

            objective.setDisplayName(finalName);

            final Team time = scoreboard.getTeam("time" + player.getName());
            time.setSuffix("§c"+Varo.playedTime.get(player) + " Min");

            final Team kills = scoreboard.getTeam("kills" + player.getName());
            kills.setSuffix("§c"+ Varo.playerData.get(player).getKills());

        });

    }

}
