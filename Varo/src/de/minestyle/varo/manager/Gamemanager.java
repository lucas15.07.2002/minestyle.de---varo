package de.minestyle.varo.manager;

import de.minestyle.varo.Varo;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class Gamemanager {

    final File file = new File("plugins//Varo", "manager.yml");
    final YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

    private static boolean started;


    public void createFile(){
        if(!file.exists()){
            try {
                configuration.options().copyDefaults(true);
                configuration.addDefault("Game.started", false);
                configuration.save(file);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveFile(){
        try {
            configuration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void setStarted(final boolean b){
        configuration.set("Game.started", b);
        started = b;
        saveFile();
    }

    public boolean getStarted(){
        return started;
    }

    public boolean getStartedFromFile(){
        return configuration.getBoolean("Game.started");
    }

    public void start(){
        startSheduler(Varo.getInstance());
    }

    private int count = 10;
    private int id;

    public void startSheduler(final JavaPlugin javaPlugin){
        id = Bukkit.getScheduler().scheduleSyncRepeatingTask(javaPlugin, () -> {

            count--;
            if(count == 0){
                Bukkit.broadcastMessage(Varo.getInstance().getPrefix() + "§aDas Spiel startet");
                setStarted(true);
                Varo.getInstance().setStarted(true);
                Bukkit.getOnlinePlayers().forEach(p -> {
                    p.setGameMode(GameMode.SURVIVAL);
                    Varo.getInstance().getScoreboard().setScoreboard(p);
                });
                Bukkit.getScheduler().cancelTask(id);
            }else {

                if(count == 45 ||  count == 30 || count == 15 || count <= 10){
                    Bukkit.broadcastMessage(Varo.getInstance().getPrefix() + "Das Spiel startet in §5" + count + " Sekunden");
                }
                Varo.getInstance().getUtils().sendActionbarToAll("§8§l« §5§lDas Spiel startet in " + count + " Sekunden §8§l»");

            }

        }, 0, 20);
    }

}
