package de.minestyle.varo.manager;

import de.minestyle.varo.Varo;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfigManager {

    final File file = new File("plugins//Varo", "teams.yml");
    final YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

    private static boolean started;


    public void createFile(){
        if(!file.exists()){
            try {

                configuration.save(file);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveFile(){
        try {
            configuration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void registerTeam(final int teamNumber, final String teamName, final String player1, final String player2){

        List<Integer> list;
        try{
            list = configuration.getIntegerList("Teams");
        }catch (Exception e){
            list = new ArrayList<>();
        }
        list.add(teamNumber);
        configuration.set("Teams", list);

        configuration.set(teamNumber + ".Teamname", teamName);
        configuration.set(teamNumber + ".player1", player1);
        configuration.set(teamNumber + ".player2", player2);

        saveFile();


    }

    public void secondversion(final int teamNumber, final String teamName, final String player1, final String player2){
        final String s = teamNumber + ":" + teamName + ":" + player1 + ":" + player2;


        List<String> list;
        try{
            list = configuration.getStringList("Teams");
        }catch (Exception e){
            list = new ArrayList<>();
        }
        list.add(s);
        configuration.set("Teams", list);
        saveFile();
        Bukkit.getConsoleSender().sendMessage(Varo.getInstance().getPrefix() + "§6§l" + s);
    }

    public boolean teamNumberExist(int teamNumber){

        boolean b = configuration.contains("" + teamNumber);
        return b;


    }

    public boolean teamNameExist(String teamName){
        List<Integer> list = configuration.getIntegerList("Teams");

        for(Integer i : list){
            String s = configuration.getString(i + ".Teamname");
            if(teamName.equalsIgnoreCase(s)){
                return true;
            }
        }
        return false;

    }

    public boolean teamPlayerExist(String playerName){

        List<Integer> list = configuration.getIntegerList("Teams");

        for(Integer i : list){
            String s = configuration.getString(i + ".player1");
            String s2 = configuration.getString(i + ".player2");
            if(playerName.equalsIgnoreCase(s) || playerName.equalsIgnoreCase(s2)){
                return true;
            }
        }
        return false;

    }

}
