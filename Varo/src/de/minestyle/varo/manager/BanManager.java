package de.minestyle.varo.manager;

import de.minestyle.varo.Varo;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class BanManager {

    private long getTimeMillis(){
        return System.currentTimeMillis();
    }

    public boolean canJoin(final String playername){
        long l = getEnd(playername);
        System.out.println(getTimeMillis());
        System.out.println(l);

        if(getTimeMillis() >=  l){
            return true;
        }

        if(extraJoin(playername) >= 1){
            removeExtraJoin(playername);
            return true;
        }

        return false;

    }

    public long getEnd(final String playername){
        return Varo.getInstance().getPlayerConfig().getConfiguration().getLong(playername + ".canlogin");
    }

    public int extraJoin(final String playername){
        return Varo.getInstance().getPlayerConfig().getExtraLogins(playername);
    }

    public void removeExtraJoin(final String playername){

        final YamlConfiguration cfg = Varo.getInstance().getPlayerConfig().configuration;
        cfg.set(playername + ".extraJoin", extraJoin(playername)-1);
        Varo.getInstance().getPlayerConfig().saveFile();


    }

    public void quit(final String playername){
        int seconds = 1*60*60*24;

        long current = getTimeMillis();
        long millis = seconds*1000;
        long end = current+millis;

        //canlogin

        Varo.getInstance().getPlayerConfig().getConfiguration().set(playername + ".canlogin", end);
        Varo.getInstance().getPlayerConfig().saveFile();
    }

    public String getReamainingTime(final String playername){
        long current = getTimeMillis();
        long end = getEnd(playername);
        long millis = end-current;

        int seconds = 0;
        int minutes = 0;
        int hours = 0;

        while (millis > 1000){
            millis-=1000;
            seconds++;
        }
        while (seconds > 60){
            seconds-=60;
            minutes++;
        }
        while (minutes > 60){
            minutes-=60;
            hours++;
        }

        return hours + " Stunde(n), " + minutes + " Minute(n), " + seconds + " Sekunde(n)";

    }

    public void cancel(final Player player){
        player.kickPlayer(Varo.getInstance().getPrefix() + "§cDu kannst nicht Joinen " +
                "\n§4Grund: §3§lDu darfst nicht aufnehmen! " +
                "\n§cVerbliebende Zeit: §5§l" + getReamainingTime(player.getName()));
    }

}
