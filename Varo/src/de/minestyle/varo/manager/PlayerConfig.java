package de.minestyle.varo.manager;

import de.minestyle.varo.Varo;
import de.minestyle.varo.utils.Playerdata;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class PlayerConfig {

    final File file = new File("plugins//Varo", "players.yml");
    final YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

    private static boolean started;


    public void createFile(){
        if(!file.exists()){
            try {

                configuration.save(file);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveFile(){
        try {
            configuration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getTeamNumber(final String playername){

        return configuration.getInt(playername + ".TeamNummer");

    }

    public int getPlayerNumber(final String playername){
        YamlConfiguration cfg = Varo.getInstance().getConfigManager().configuration;

        List<Integer> list = cfg.getIntegerList("Teams");

        for(Integer i : list){
            String s = cfg.getString(i + ".player1");
            String s2 = cfg.getString(i + ".player2");
            if(playername.equalsIgnoreCase(s)){
                return 1;
            }else if(playername.equalsIgnoreCase(s2)){
                return 2;
            }
        }
        return -1;
    }

    public int getKills(final String playername){
        return configuration.getInt(playername  + ".kills");
    }

    //player.sendMessage(Varo.getInstance().getPrefix() +);

    public int getStrikes(final String playername){
        return configuration.getInt(playername  + ".Strikes");
    }

    public boolean isDead(final String playername){
        return configuration.getBoolean(playername  + ".died");
    }

    public long getCanLogin(final String playername){
        return configuration.getLong(playername + ".canlogin");
    }

    public int getExtraLogins(final String playername){
        return configuration.getInt(playername  + ".extraJoin");
    }

    public String getTeamMate(final String playername){
        final YamlConfiguration cfg = Varo.getInstance().getConfigManager().configuration;

        List<Integer> list = cfg.getIntegerList("Teams");

        for(Integer i : list){
            String s = cfg.getString(i + ".player1");
            String s2 = cfg.getString(i + ".player2");
            if(playername.equalsIgnoreCase(s)){
                return s2;
            }else if(playername.equalsIgnoreCase(s2)){
                return s;
            }
        }
        return "";
    }

    public String getTeam(final String playername){
        final YamlConfiguration cfg = Varo.getInstance().getConfigManager().configuration;

        List<Integer> list = cfg.getIntegerList("Teams");

        for(Integer i : list){
            String s = cfg.getString(i + ".player1");
            String s2 = cfg.getString(i + ".player2");
            System.out.println("" + s + ":" + s2);
            if(playername.equalsIgnoreCase(s) || playername.equalsIgnoreCase(s2)){
               return cfg.getString(i + ".Teamname");
            }
        }
        return "";
    }

    public boolean savePlayerConfig(final Playerdata playerdata, final Player player){
        if(!Varo.playerData.containsValue(playerdata)){
            Bukkit.getConsoleSender().sendMessage(Varo.getInstance().getPrefix() + "§cDer Spieler " + player.getName() + " §cwurde nicht regristriert!");
        }
        return true;
    }

    public void createPlayer(final Player player){

        YamlConfiguration cfg = Varo.getInstance().getConfigManager().configuration;

        int teamNumber = -1;
        String teamName = "";
        String playername = player.getName();
        int kills = 0;
        boolean died = false;
        int strikes = 0;

        List<Integer> list = cfg.getIntegerList("Teams");

        for(Integer i : list){
            String s = cfg.getString(i + ".player1");
            String s2 = cfg.getString(i + ".player2");
            System.out.println("" + s + ":" + s2);
            if(playername.equalsIgnoreCase(s) || playername.equalsIgnoreCase(s2)){
                teamNumber = i;
                teamName = cfg.getString(i+".Teamname");
            }
        }
        configuration.set(playername + ".TeamNummer", teamNumber);
        configuration.set(playername + ".TeamName", teamName);
        configuration.set(playername + ".died", died);
        configuration.set(playername + ".kills", kills);
        configuration.set(playername+ ".Strikes", strikes);
        configuration.set(playername+ ".canlogin", System.currentTimeMillis());
        configuration.set(playername+ ".extraJoin", 3);

        saveFile();


    }

    public boolean playerExist(final Player player){
        return configuration.contains(player.getName());
    }

    public YamlConfiguration getConfiguration() {
        return configuration;
    }

}
