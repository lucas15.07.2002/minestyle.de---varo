package de.minestyle.varo.manager;

import de.minestyle.varo.Varo;
import de.minestyle.varo.commands.NoVo;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class LocationManager {

    final File file = new File("plugins//Varo", "locations.yml");
    final YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

    private static boolean started;


    public void createFile(){
        if(!file.exists()){
            try {

                configuration.save(file);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveFile(){
        try {
            configuration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean setLocation(final Player player, final int teamNumber, final int playerNumber){

        final Location location = player.getLocation();
        double x, y, z;
        float yaw, pitch;
        String world;

        world = location.getWorld().getName();
        x = location.getX();
        y = location.getY();
        z = location.getZ();
        yaw = location.getYaw();
        pitch = location.getPitch();


        configuration.set(teamNumber + "." + playerNumber+ ".world", world);
        configuration.set(teamNumber + "." + playerNumber+ ".x", x);
        configuration.set(teamNumber + "." + playerNumber+ ".y", y);
        configuration.set(teamNumber + "." + playerNumber+ ".z", z);
        configuration.set(teamNumber + "." + playerNumber+ ".yaw", yaw);
        configuration.set(teamNumber + "." + playerNumber+ ".pitch", pitch);
        saveFile();

        return true;
    }

    public Location getLocation(final int teamNumber, int playerNumber) throws NullPointerException{

        double x, y, z;
        float yaw, pitch;
        World world;

        world = Bukkit.getWorld(configuration.getString(teamNumber + "." + playerNumber+ ".world"));
        x = configuration.getDouble(teamNumber + "." + playerNumber+ ".x");
        y = configuration.getDouble(teamNumber + "." + playerNumber+ ".y");
        z = configuration.getDouble(teamNumber + "." + playerNumber+ ".z");
        yaw = (float) configuration.getDouble(teamNumber + "." + playerNumber+ ".yaw");
        pitch = (float) configuration.getDouble(teamNumber + "." + playerNumber+ ".pitch");

        final Location location = new Location(world, x, y, z, yaw, pitch);
        return location;

    }

    public void teleportToSpawn(final Player player){
        final int teamnumber = Varo.getInstance().getPlayerConfig().getTeamNumber(player.getName());
        final int playernumber = Varo.getInstance().getPlayerConfig().getPlayerNumber(player.getName());

        if(playernumber == -1 || teamnumber == -1){
            if(player.hasPermission("Varo.ADMIN")){
                Varo.getInstance().setItem(player);
            }else {
                player.kickPlayer("§cDu wurdest noch nicht configuriert!");
            }
            return;
        }

        try{
            player.teleport(getLocation(teamnumber, playernumber));
        }catch (NullPointerException e){
            Varo.getInstance().getPlayerConfig().createPlayer(player);
        }


    }

}
